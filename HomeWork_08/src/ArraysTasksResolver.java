package ArrayTasks;

public class ArraysTasksResolver {
    static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%d ", array[i]);
        }
        System.out.printf("\nfrom: %d ", from);
        System.out.printf("\nto: %d ", to);

        int result = task.resolve(array, from, to);
        System.out.printf("\nresult: %d\n", result);
    }
}
