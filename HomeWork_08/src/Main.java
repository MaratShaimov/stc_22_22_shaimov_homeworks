package ArrayTasks;

public class Main {
    public static void main(String[] args) {
        int[] arr = new int[] {1,2,37,4};

        // Посчитать сумму элементов массива в заданном промежутке
        ArrayTask taskSum = ((array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            return sum;
        });

        // Посчитать сумму цифр элемента с максимальным значением в заданном промежутке.
        ArrayTask taskDigitsSum = ((array, from, to) -> {
            int max = Integer.MIN_VALUE;
            for (int i = from; i <= to; i++) {
                if (array[i] > max)
                    max = array[i];
            }

            int sum = 0;
            while (max > 0 ) {
                sum += max % 10;
                max /= 10;
            }
            return sum;
        });

        // демонстрация работы каждого лямбда-выражения с помощью ArraysTasksResolver
        ArraysTasksResolver.resolveTask(arr, taskSum, 0, 3);
        ArraysTasksResolver.resolveTask(arr, taskDigitsSum, 0, 3);
    }
}