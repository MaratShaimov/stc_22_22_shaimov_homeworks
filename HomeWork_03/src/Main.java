import java.util.Arrays;
import java.util.Scanner;

class Main {
    static int extrema(int[] a, int n) {
        int count = 0;
        for (int i = 1; i < n - 1; i++) {
            if (a[i] > a[i - 1] && a[i] > a[i + 1])
                count += 1;
            if (a[i] < a[i - 1] && a[i] < a[i + 1])
                count += 1;

        }
        return count;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int sum = 0;
        for (int j : array) {
            sum = sum + j;
        }

        int n = array.length;


        System.out.println(Arrays.toString(array));
        System.out.println(sum);
        System.out.println(extrema(array, n));
    }

}
