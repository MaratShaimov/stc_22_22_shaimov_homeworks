public class ArrayList<T> implements List<T> {

    private final static int DEFAULT_ARRAY_SIZE = 10;


    private T[] elements;


    private int count;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    @Override
    public void add(T element) {

        if (isFull()) {

            resize();
        }
        elements[count] = element;
        count++;
    }

    private void resize() {

        int currentLength = elements.length;

        int newLength = currentLength + currentLength / 2;

        T[] newElements = (T[]) new Object[newLength];

        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }

        this.elements = newElements;
    }

    private boolean isFull() {
        return count == elements.length;
    }

    @Override
    public void remove(T element) {
        int index = indexOf(element);
        if (index >= 0) {
            System.arraycopy(elements, index + 1, elements, index, count - index - 1);
            count--;
            elements[count] = null;
        }
    }

    public int indexOf(Object o) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(o)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(T element) {

        for (int i = 0; i < count; i++) {

            if (elements[i].equals(element)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        if (index > count) {
            throw new IndexOutOfBoundsException();
        }

        System.arraycopy(elements, index + 1, elements, index, count - index - 1);
        elements[--count] = null;
        resize();
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return null;
    }

    private class ArrayListIterator implements Iterator<T> {

        private int currentIndex = 0;

        @Override
        public T next() {
            T value = elements[currentIndex];
            currentIndex++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < count;
        }
    }
    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }
}