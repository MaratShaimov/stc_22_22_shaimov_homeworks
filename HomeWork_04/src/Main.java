import java.util.Scanner;
public class Main {

    public static int getSumOnInterval(int[] array, int left, int right) {
        int len = array.length;
        if (left > right || right >= len || left >= len)
            return -1;

        int sum = 0;
        for (int i = left; i <= right; i++) {
            sum += array[i];
        }
        return sum;
    }


    public static void printEvenNumbers(int[] array) {
        int len = array.length;
        for (int i = 0; i < len; i++) {
            if (array[i] % 2 == 0)
                System.out.printf("%d ", array[i]);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner( System.in );
        // Ввод массива
        System.out.println("Введите количество элементов в массиве: ");
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.printf("Введите элемент №%d: ", i);
            array[i] = scanner.nextInt();
        }

        System.out.println("Введите значение левого интервала: ");
        int left = scanner.nextInt();
        System.out.println("Введите значение правого интервала: ");
        int right = scanner.nextInt();
        System.out.println("Результат (сумма):\n" + getSumOnInterval(array, left, right));
        System.out.println("Чётные элементы: ");
        printEvenNumbers(array);
    }
}