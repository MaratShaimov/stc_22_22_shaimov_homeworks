package Products;

import StreamApi.Car;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    String fileName;
    String delimeter = "_";

    List<Product> products;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
        products = new ArrayList();

        readFile();
    }

    void readFile() {
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            for(String line; (line = br.readLine()) != null; ) {
                String[] parts = line.split(delimeter);
                Product product  = new Product(Integer.parseInt(parts[0]), parts[1], Double.parseDouble(parts[2]), Integer.parseInt(parts[3]));
                products.add(product);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void rewriteFile() {
        String content = "";
        for (int i = 0; i < products.size(); i++) {
            Product product = products.get(i);
            content += String.format("%d%s%s%s%.2f%s%d\n", product.id, delimeter, product.name, delimeter, product.price, delimeter, product.amount);
        }

        try {
            FileWriter writerObj = new FileWriter(fileName, false);
            writerObj.write(content);
            writerObj.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Product findById(Integer id) {
        Product product = products.stream()
                .filter(p -> p.id == id)
                .findAny()
                .orElse(null);

        return product;
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        List<Product> result = products.stream()
                .filter(p -> p.name.toLowerCase().contains(title.toLowerCase())).collect(Collectors.toList());

        return result;
    }

    @Override
    public void update(Product product) {
        rewriteFile();
    }
}