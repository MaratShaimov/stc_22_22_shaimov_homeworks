CREATE TABLE `User` (
	`firstname` VARCHAR NOT NULL,
	`lastname` VARCHAR NOT NULL,
	`phone_number` VARCHAR NOT NULL,
	`experience` INT NOT NULL,
	`age` INT NOT NULL,
	`has_driver_license` BOOLEAN NOT NULL,
	`rights_category` VARCHAR(2) NOT NULL,
	`raiting` INT(1) NOT NULL
);

CREATE TABLE `Car` (
	`model` VARCHAR NOT NULL,
	`color` VARCHAR NOT NULL,
	`number` VARCHAR NOT NULL,
	`owner_id` INT NOT NULL
);

CREATE TABLE `Ride` (
	`driver_id` INT NOT NULL,
	`car_id` INT NOT NULL,
	`date` DATE NOT NULL,
	`duration_hours` DOUBLE NOT NULL
);