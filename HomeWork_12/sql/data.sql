INSERT INTO User VALUES ("Ilya", "Berezin", "87150465232", 2, 22, True, "BE", 5);
INSERT INTO User VALUES ("Leonid", "Pavlov", "87598706511", 3, 25, True, "B", 5);
INSERT INTO User VALUES ("Maksim", "Ivanov", "82022506356", 0, 18, False, "A", 4);
INSERT INTO User VALUES ("Polina", "Mitrofanova", "88723917307", 2, 26, True, "A", 5);
INSERT INTO User VALUES ("Fedor", "Petrov", "89503465750", 2, 22, True, "B", 5);

INSERT INTO Car VALUES ("Tesla S", "White", "o294xk", 1);
INSERT INTO Car VALUES ("Honda Solaris", "White", "m352om", 2);
INSERT INTO Car VALUES ("BMW X5", "Black", "l344ok", 3);
INSERT INTO Car VALUES ("Kia Rio", "Red", "a177la", 4);
INSERT INTO Car VALUES ("Chevrolet Cruze", "Green", "p390tt", 5);

INSERT INTO Ride VALUES (1, 1, '07/11/2022', 2.5);
INSERT INTO Ride VALUES (2, 2, '08/11/2022', 3);
INSERT INTO Ride VALUES (3, 3, '09/11/2022', 1);
INSERT INTO Ride VALUES (4, 4, '09/11/2022', 1);
INSERT INTO Ride VALUES (5, 5, '12/11/2022', 2);