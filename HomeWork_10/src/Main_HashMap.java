import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//Домашнее задание по теме “HashMap”
public class Main_HashMap {
    public static void main(String[] args) {
        Scanner scanner = new Scanner( System.in );
        // Ввод массива
        System.out.println("Введите строку для анализа: ");
        String str = scanner.nextLine();

        String[] words = str.split(" ");
        HashMap<String, Integer> hashMap = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            Integer currentValue = hashMap.get(words[i]);
            if (currentValue == null)
                currentValue = 0;
            hashMap.put(words[i], currentValue + 1);
        }

        int max = Integer.MIN_VALUE;
        String maxWord = "";
        for (Map.Entry<String, Integer> entry: hashMap.entrySet()) {
            Integer value = entry.getValue();
            if (value > max) {
                max = value;
                maxWord = entry.getKey();
            }
        }

        System.out.println(maxWord + " " + max);
    }
}