package Figures;

public class Circle extends Figure {

    protected int r;


    double getPerimeter() {
        return 2 * Math.PI * r;
    }
}
