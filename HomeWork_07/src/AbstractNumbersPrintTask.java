package Tasks;

public abstract class AbstractNumbersPrintTask implements Task {
    int from;
    int to;

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public abstract void complete();
}
