package Tasks;

public class OddNumbersPrintTask extends  AbstractNumbersPrintTask {

    public  OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public void complete() {
        for (int i = from; i <= to; i++) {
            if (Math.abs(i % 2) == 1) {
                System.out.printf("%d ", i);
            }
        }
        System.out.println();
    }
}
