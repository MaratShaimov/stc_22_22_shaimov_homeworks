package Tasks;

import java.util.Scanner;

public class Main {
    static void completeAllTasks(Task[] tasks) {
        for (Task task : tasks) {
            task.complete();
        }
    }

    public static void main(String[] args) {
        EvenNumbersPrintTask evenTask1 = new EvenNumbersPrintTask(-5, 10);
        EvenNumbersPrintTask evenTask2 = new EvenNumbersPrintTask(0, 6);
        OddNumbersPrintTask oddTask1 = new OddNumbersPrintTask(0, 10);
        OddNumbersPrintTask oddTask2 = new OddNumbersPrintTask(-7, 10);

        Task[] tasks = new Task[4];
        tasks[0] = evenTask1;
        tasks[1] = evenTask2;
        tasks[2] = oddTask1;
        tasks[3] = oddTask2;

        completeAllTasks(tasks);
    }
}