package StreamApi;

public class Car {
    String number;
    String name;
    String color;
    int probeg;
    int price;

    public Car(String number, String name, String color, int probeg, int price) {
        this.number = number;
        this.name = name;
        this.color = color;
        this.probeg = probeg;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public int getPrice() {
        return price;
    }
}