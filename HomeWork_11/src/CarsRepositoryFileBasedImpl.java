package StreamApi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository {

    String fileName;
    String delimeter = "_";

    List<Car> cars;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
        cars = new ArrayList();

        readFile();
    }

    void readFile() {
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            for(String line; (line = br.readLine()) != null; ) {
                String[] parts = line.split(delimeter);
                Car car = new Car(parts[0], parts[1], parts[2], Integer.parseInt(parts[3]), Integer.parseInt(parts[4]));
                cars.add(car);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> getBlackOrNewCarNums() {
        return cars.stream().filter(car -> car.color == "black" || car.probeg == 0)
                .map(Car::getNumber).collect(Collectors.toList());
    }

    @Override
    public List<Car> getUniqCars() {
        return cars.stream().filter(car -> car.price >= 700000 || car.price <= 800000)
                .distinct().collect(Collectors.toList());
    }

    @Override
    public String getCheapestCarColor() {
        var filteredCars = cars.stream().sorted((c1, c2)->c1.price-c2.price).limit(1)
                .collect(Collectors.toList());
        return filteredCars.get(0).color;
    }

    @Override
    public double getAverageCamryPrice() {
        return cars.stream().filter(car->car.name == "camry").mapToDouble(Car::getPrice).average().orElse(Double.NaN);
    }
}