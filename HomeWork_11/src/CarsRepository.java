package StreamApi;

import java.util.List;

public interface CarsRepository
{
    List<String> getBlackOrNewCarNums();

    List<Car> getUniqCars();

    String getCheapestCarColor();

    double getAverageCamryPrice();
}
