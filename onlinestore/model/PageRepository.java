package com.marat.onlinestore.model;

import com.marat.onlinestore.model.data.Page;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PageRepository extends JpaRepository<Page, Integer> {

    Page findBySlug(String slug);

    Page findBySlugAndIdNot(String slug, int id);

}
