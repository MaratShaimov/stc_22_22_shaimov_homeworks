package com.marat.onlinestore.model;

import com.marat.onlinestore.model.data.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    Category findByName(String name);

    Category findBySlug(String slug);
}
