public class Bankomat {

    int moneyLeft;
    int maxMoneyGiveValue;
    int maxMoneyCapacity;
    int operationsCount;

    int giveMoney(int value) {

        if (value > maxMoneyGiveValue)
            value = maxMoneyGiveValue;


        if (value > moneyLeft)
            value = moneyLeft;


        moneyLeft -= value;
        operationsCount++;
        return value;
    }

    int putMoney(int value) {
        int desiredMoneyLeft = moneyLeft + value;

        if (desiredMoneyLeft > maxMoneyCapacity) {
            moneyLeft = maxMoneyCapacity;
            operationsCount++;

            return desiredMoneyLeft - maxMoneyCapacity;
        }

        operationsCount++;
        return 0;
    }
}